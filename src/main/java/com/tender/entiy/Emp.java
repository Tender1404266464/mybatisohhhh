package com.tender.entiy;


public class Emp {

  private long eno;
  private String ename;
  private String esex;
  private double salary;
  private String pwd;
  private long dno;

  public Emp(long eno, String ename, String esex, double salary, String pwd, long dno) {
    this.eno = eno;
    this.ename = ename;
    this.esex = esex;
    this.salary = salary;
    this.pwd = pwd;
    this.dno = dno;
  }

  public Emp() {
  }

  @Override
  public String toString() {
    return "Emp{" +
            "eno=" + eno +
            ", ename='" + ename + '\'' +
            ", esex='" + esex + '\'' +
            ", salary=" + salary +
            ", pwd='" + pwd + '\'' +
            ", dno=" + dno +
            '}';
  }

  public long getEno() {
    return eno;
  }

  public void setEno(long eno) {
    this.eno = eno;
  }


  public String getEname() {
    return ename;
  }

  public void setEname(String ename) {
    this.ename = ename;
  }


  public String getEsex() {
    return esex;
  }

  public void setEsex(String esex) {
    this.esex = esex;
  }


  public double getSalary() {
    return salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }


  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }


  public long getDno() {
    return dno;
  }

  public void setDno(long dno) {
    this.dno = dno;
  }

}
