package com.tender.web.controller;


import com.github.pagehelper.PageInfo;
import com.tender.entiy.Emp;
import com.tender.service.EmpService;
import com.tender.service.impl.EmpServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/page")
public class EmpContorller extends HttpServlet {
    EmpService es= new EmpServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String pageIndex = req.getParameter("pageIndex");

        if (pageIndex==null||pageIndex.equals("")){
            pageIndex="1";
        }
        PageInfo<Emp> empBypages = es.getEmpBypages(Integer.valueOf(pageIndex));
        System.out.println(empBypages);

        req.setAttribute("empInfo", empBypages);
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
