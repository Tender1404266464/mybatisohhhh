package com.tender.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tender.dao.EmpMapper;
import com.tender.entiy.Emp;
import com.tender.service.EmpService;
import com.tender.util.MyBatisUtils;

import java.util.List;

public class EmpServiceImpl implements EmpService {
    /**
    * Description: <br/>
    * date:<br/>
    * @author: 曦光君 <br/>
    * @since JDK 1.8
     * @zhushi： 查询所有数据
    */
    @Override
    public List<Emp> getAll() {
        EmpMapper mapper = MyBatisUtils.getMapper(EmpMapper.class);
        List<Emp> all = mapper.getAll();
        MyBatisUtils.commit();
        return all;
    }
/**
* Description: <br/>
* date:<br/>
* @author: 曦光君 <br/>
* @since JDK 1.8
 * @zhushi： 获取页数
*/
    @Override
    public PageInfo<Emp> getEmpBypages(int pageIndex) {
        EmpMapper mapper = MyBatisUtils.getMapper(EmpMapper.class);
/**
* Description: <br/>
* date:<br/>
* @author: 曦光君 <br/>
* @since JDK 1.8
 * @zhushi： 设置每页最多十条数据
*/
        PageHelper.startPage(pageIndex,10);
//查询结果返回出去
        List<Emp> all = mapper.getAll();

        PageInfo<Emp> emps = new PageInfo<>(all);

        return emps;
    }
}
