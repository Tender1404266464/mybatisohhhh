package com.tender.service;

import com.github.pagehelper.PageInfo;
import com.tender.entiy.Emp;

import java.util.List;

public interface EmpService  {
    List<Emp> getAll();

    PageInfo<Emp> getEmpBypages(int pageIndex);
}
