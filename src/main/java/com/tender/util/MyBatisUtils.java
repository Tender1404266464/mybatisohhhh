package com.tender.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
* Description: <br/>
* date:<br/>
* @author: 曦光君 <br/>
* @since JDK 1.8
 * @zhushi： 工具类，用来处理数据库的连接，事务的开启提交回滚和关闭连接
*/
public class MyBatisUtils {
    /**
    * Description: <br/>
    * date:<br/>
    * @author: 曦光君 <br/>
    * @since JDK 1.8
     * @zhushi： //获得SqlSession工厂
    */
    private static SqlSessionFactory factory;

    /**
    * Description: <br/>
    * date:<br/>
    * @author: 曦光君 <br/>
    * @since JDK 1.8
     * @zhushi： //创建ThreadLocal绑定当前线程中的SqlSession对象
    */
    private static final ThreadLocal<SqlSession> tl = new ThreadLocal<SqlSession>();
/**
* Description: <br/>
* date:<br/>
* @author: 曦光君 <br/>
* @since JDK 1.8
 * @zhushi： 绑定rescources里面的mybatisxxxx。xml文件
*/
    static {
        try {
            InputStream is = Resources.getResourceAsStream("mybatisceshi.xml");
            factory = new SqlSessionFactoryBuilder().build(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //获得连接（从tl中获得当前线程SqlSession）
    private static SqlSession openSession(){
        SqlSession session = tl.get();
        if(session == null){
            session = factory.openSession();
            tl.set(session);
        }
        return session;
    }

    //释放连接（释放当前线程中的SqlSession）
    private static void closeSession(){
        SqlSession session = tl.get();
        session.close();
        tl.remove();
    }

    //提交事务（提交当前线程中的SqlSession所管理的事务）
    public static void commit(){
        SqlSession session = openSession();
        session.commit();
        closeSession();
    }

    //回滚事务（回滚当前线程中的SqlSession所管理的事务）
    public static void rollback(){
        SqlSession session = openSession();
        session.rollback();
        closeSession();
    }

    //获得接口实现类对象   （这样我们在创建测试类或者其他的时候可以直接放一个要处理的类进去）
//    不需要再重新写那么多东西
    public static <T extends Object> T getMapper(Class<T> clazz){
        SqlSession session = openSession();
        return session.getMapper(clazz);
    }
}
