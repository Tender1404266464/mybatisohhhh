<%--
  Created by IntelliJ IDEA.
  User: 86156
  Date: 2021/11/10
  Time: 19:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>展示</title>
</head>
<body>

            <div style="align-content: center">
                <from >
                    <table cellpadding="10px" border="2px" style="border: aqua" cellspacing="10px">
                        <tr>
                            <td>员工编号</td>
                            <td>员工姓名</td>

                            <td>员工性别</td>
                            <td>员工工资</td>
                            <td>员工密码</td>
                            <td>员工部门编号</td>
                            <td>操作</td>
                        </tr>
                        <c:forEach items="${empInfo.list}" var="e">
                            <tr>
                                <td>${e.eno}</td>
                                <td>${e.ename}</td>
                                <td>${e.esex}</td>
                                <td>${e.salary}</td>
                                <td>${e.pwd}</td>
                                <td>${e.dno}</td>
                                <td><a href="#">删除</a></td>


                            </tr>


                        </c:forEach>
                        <tr>
                            <td>
                                <a href="${pageContext.request.contextPath}/page?pageIndex=1">首页</a>
                                <a href="${pageContext.request.contextPath}/page?pageIndex=${empInfo.pageNum-1}">上一页</a>

                                <c:forEach items="${empInfo.navigatepageNums}" var="i" >
                                    <a href="${pageContext.request.contextPath}/page?pageIndex=${i}">${i}</a>
                                </c:forEach>

                                <a href="${pageContext.request.contextPath}/page?pageIndex=${empInfo.pageNum+1}">下一页</a>
                                <a href="${pageContext.request.contextPath}/page?pageIndex=${empInfo.pages}">尾页</a>
                                &nbsp; ${empInfo.pageNum}/${empInfo.pages}
                            </td>


                        </tr>
                    </table>

                </from>

        
        
    </div>
</body>
</html>
